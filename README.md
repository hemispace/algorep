# Build & Run instructions

Preferably build using cmake or else docker, but be aware that due to a limited shared memory,
docker processes might crash if there are too much.

## Using cmake
### Prerequisite
#### Install:
- gcc==10 (gcc9 may work too)
- cmake >= 3.10
- boost libraries
- openmpi libraries
### Build
```shell script
# From project root
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j8
```
### Basic Run Pipeline

Here is an example of a basic pipeline to run our algorithm using the CLI.
For information about each command, run 'help' in the CLI.

```shell script
# From build directory
./mpicli
> info
> run
> start
> progress
> fetch
> check
> diff
> kill
```

## Using docker
### Prerequisite
- Install docker and docker-compose
- Have a running docker daemon
### Build & Run
```shell script
# From projet root
docker-compose run --rm algorep
```
> NOTE: If the previous command does not automatically build the docker image, please run `docker-compose build`.
>
> WARNING: The mpi application may crash if too many processes are launched, due to a lack of shared memory.
> The shared memory has already been increased inside the docker-compose.yml but can be furthermore increased
> if necessary by changing the parameter `shm_size`.

**Warning**
We've noticed a bug with the CLI in XTerm which forces to write the commands
perfectly, using backspaces does not register commands correctly.
An alternative is to run the CLI in urxvt where it works fine.

# Credits
## 3rdparty tools
- github:daniele77/cli A cross-platform header only C++14 library for interactive command line interfaces (Cisco style)

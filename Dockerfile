FROM ubuntu:groovy

WORKDIR /app

RUN apt-get update && apt-get install -y build-essential cmake libboost-all-dev libopenmpi-dev openmpi-bin

COPY . .

ENV OMPI_ALLOW_RUN_AS_ROOT 1
ENV OMPI_ALLOW_RUN_AS_ROOT_CONFIRM 1

RUN mkdir build && cd build && cmake .. -DCMAKE_BUILD_TYPE=Release && make -j8

WORKDIR /app/build

CMD ["/app/build/mpicli"]

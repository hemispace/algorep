#include <cli/cli.h>
#include <cli/clilocalsession.h>
#include <optional>
#include "pstream.h"

enum Log_Method {
    CHECK,
    DIFF,
    PROGRESS,
    CAT
};

// TODO: Add more parameters
struct Parameters {
    unsigned clients{12};
    unsigned servers{4};
    unsigned requests_per_client{100};
    unsigned node_rank{0};

    [[nodiscard]] std::string get_run_cmd(std::string const& bin_path) const noexcept {
        std::stringstream ss;
        unsigned nodes = clients + servers + 1;
        ss << "mpirun"
           << " -H localhost:" << nodes
           << " --stdin " << nodes - 1
           << " --np " << nodes << ' ' << bin_path + "mpiapp"
           << " -c " << clients
           << " -s " << servers
           << " -r " << requests_per_client;
        return ss.str();
    }

    [[nodiscard]] std::string get_log_utils_cmd(std::string const& bin_path, Log_Method method) const noexcept {
        std::stringstream ss;
        ss << bin_path + "log_check"
           << " -p " << bin_path + "logs/"
           << " -s " << servers;
        switch (method) {
            case Log_Method::CHECK:
                ss << " --check";
                break;
            case Log_Method::DIFF:
                ss << " --diff";;
                break;
            case Log_Method::PROGRESS:
                ss << " -c " << clients
                   << " -r " << requests_per_client
                   << " --progress";
                break;
            case Log_Method::CAT:
                ss << " -n " << node_rank
                   << " --cat";
                break;
        }

        return ss.str();
    }
};

std::ostream& operator<<(std::ostream& os, Parameters const& p) {
    return os
            << "Number of clients: " << p.clients << '\n'
            << "Number of servers: " << p.servers << '\n'
            << "Number of per-client requests: " << p.requests_per_client << '\n';
}

void run_cmd(std::ostream& os, std::string const& cmd) {
    os << "\tRunning: " << cmd << "\n\n";
    redi::ipstream proc(cmd, redi::pstreams::pstdout | redi::pstreams::pstderr);
    std::string line;
    while (std::getline(proc.out(), line))
        os << "stdout: " << line << '\n';
    if (proc.eof() && proc.fail())
        proc.clear();
    while (std::getline(proc.err(), line))
        os << "stderr: " << line << '\n';
}

redi::pstream make_proc(std::string const& cmd) {
    redi::pstream proc(cmd, redi::pstreams::pstdout | redi::pstreams::pstderr | redi::pstreams::pstdin);
    return proc;
}

auto main([[maybe_unused]]int argc, char* argv[]) -> int {
    std::string bin_path = argv[0];
    if (bin_path.length() >= 6)
        bin_path = bin_path.substr(0, bin_path.length() - 6);
    else
        bin_path = "./";

    std::optional<redi::pstream> app;
    auto p = Parameters{};
    auto menu = std::make_unique<cli::Menu>("mpicli");
    menu->Insert("info",
                 [&p](std::ostream& out) { out << p << '\n'; },
                 "Show current configuration"
    );
    menu->Insert(
            "clients",
            [&p](std::ostream& out, unsigned n) {
                p.clients = n;
                out << "Successfully set client number to " << p.clients << "\n";
            },
            "Change the number of clients"
    );
    menu->Insert(
            "servers",
            [&p](std::ostream& out, unsigned n) {
                p.servers = n;
                out << "Successfully set servers number to " << p.servers << "\n";
            },
            "Change the number of servers"
    );
    menu->Insert(
            "requests",
            [&p](std::ostream& out, unsigned n) {
                p.requests_per_client = n;
                out << "Successfully set requests per client to " << p.requests_per_client << "\n";
            },
            "Change the number of requests each client makes in a run"
    );
    menu->Insert("run",
                 [&p, &bin_path, &app](std::ostream& os) {
                     const auto cmd = p.get_run_cmd(bin_path);
                     if (app) {
                         os << "Killing previous instance of mpiapp...\n\n";
                         app->rdbuf()->kill();
                         app.reset();
                     }
                     app = make_proc(cmd);
                     os << "Launched mpiapp with " << p.clients << " clients and " << p.servers << " servers\n"
                        << "Tip: Use 'servers' and 'clients' to change these, and 'info' to read the config\n"
                        << "Now run 'start' or mess with the REPL before if you want (crash, speed)\n";
                 },
                 "Run mpiapp"
    );
    menu->Insert("kill", [&app](std::ostream& os) {
                     if (app) {
                         os << "Killing instance of mpiapp...\n\n";
                         app->rdbuf()->kill();
                         app.reset();
                     } else
                         os << "mpiapp is not running, continue...\n";
                 },
                 "Kill mpiapp"
    );
    menu->Insert("start", [&app](std::ostream& os) {
                     if (app) {
                         os << "Starting... Run 'progress' to monitor progress, 'fetch' to get logs, 'check' and 'diff' to check validity\n";
                         app.value() << "start\n";
                         app->flush();
                     } else
                         os << "mpiapp is not running, use 'run'\n";
                 },
                 "Order the REPL to run START (clients will start making requests)"
    );
    menu->Insert("speed", [&p, &app](std::ostream& os, unsigned rank, std::string speed) {
                     if (app) {
                         std::transform(speed.begin(), speed.end(), speed.begin(), ::toupper);
                         if (speed != "HIGH" && speed != "MEDIUM" && speed != "LOW")
                             os << "Invalid value for 'speed': " << speed
                                << ". Please use one of the following: (HIGH, MEDIUM, LOW)\n";
                         else if (rank >= p.servers) {
                             os << "Node rank: " << rank
                                << " exceeds number of servers: " << p.servers
                                << ". Please select a correct node rank.\n";
                         } else {
                             int speed_lvl = 0;
                             if (speed == "HIGH") speed_lvl = 1;
                             if (speed == "MEDIUM") speed_lvl = 2;
                             if (speed == "LOW") speed_lvl = 3;
                             os << "Changing speed of " << rank << " to " << speed << "...\n";
                             app.value() << "speed\n" << rank << " " << speed_lvl << "\n";
                             app->flush();
                         }
                     } else
                         os << "mpiapp is not running, use 'run'\n";
                 },
                 "Order the REPL to run SPEED (modify the execution speed of a server to HIGH, MEDIUM or LOW)"
    );
    menu->Insert("crash", [&p, &app](std::ostream& os, unsigned rank) {
                     if (app) {
                         if (rank >= p.servers) {
                             os << "Node rank: " << rank
                                << " exceeds number of servers: " << p.servers
                                << ". Please select a correct node rank.\n";
                         } else {
                             os << "Asking " << rank << " to crash...\n";
                             app.value() << "crash\n" << rank << "\n";
                             app->flush();
                         }
                     } else
                         os << "mpiapp is not running, use 'run'\n";
                 },
                 "Order the REPL to run CRASH (simulate the crash of a node)"
    );
    menu->Insert("recover", [&p, &app](std::ostream& os, unsigned rank) {
                     if (app) {
                         if (rank >= p.servers) {
                             os << "Node rank: " << rank
                                << " exceeds number of servers: " << p.servers
                                << ". Please select a correct node rank.\n";
                         } else {
                             os << "Asking " << rank << " to initiate recovery\n";
                             app.value() << "recover\n" << rank << "\n";
                             app->flush();
                         }
                     } else
                         os << "mpiapp is not running, use 'run'\n";
                 },
                 "Order the REPL to run RECOVER (ask a server to recover from crash)");
    menu->Insert("fetch", [&app](std::ostream& os) {
                     if (app) {
                         std::streamsize n;
                         char buf[1024];
                         while ((n = app->out().readsome(buf, sizeof(buf))) > 0)
                             os.write(buf, n);
                     } else
                         os << "mpiapp is not running, use 'run'\n";
                 },
                 "Fetch info et commit logs from mpiapp to stdout"
    );
    menu->Insert("errors", [&app](std::ostream& os) {
                     if (app) {
                         std::streamsize n;
                         char buf[1024];
                         while ((n = app->err().readsome(buf, sizeof(buf))) > 0)
                             os.write(buf, n);
                     } else
                         os << "mpiapp is not running, use 'run'\n";
                 },
                 "Fetch stderr logs from mpiapp to stdout (if there is any)"
    );
    menu->Insert("check",
                 [&p, &bin_path](std::ostream& os) {
                     const auto cmd = p.get_log_utils_cmd(bin_path, Log_Method::CHECK);
                     run_cmd(os, cmd);
                 },
                 "Check logs from last run"
    );
    menu->Insert("diff",
                 [&p, &bin_path](std::ostream& os) {
                     const auto cmd = p.get_log_utils_cmd(bin_path, Log_Method::DIFF);
                     run_cmd(os, cmd);
                 },
                 "Display potential diffs of logs from last run"
    );
    menu->Insert("progress",
                 [&p, &bin_path](std::ostream& os) {
                     const auto cmd = p.get_log_utils_cmd(bin_path, Log_Method::PROGRESS);
                     run_cmd(os, cmd);
                 },
                 "Display current progress of the mpiapp"
    );
    menu->Insert("cat",
                 [&p, &bin_path](std::ostream& os, unsigned rank) {
                    
                     if (rank >= p.clients + p.servers + 1)
                         os << "Node rank: " << rank
                            << " exceeds current number of nodes: "
                            << p.clients + p.servers + 1 << ". "
                            << "Please select a correct node rank.\n";
                     else {
                         p.node_rank = rank;
                         const auto cmd = p.get_log_utils_cmd(bin_path, Log_Method::CAT);
                         std::string node_type;
                         if (rank < p.servers)
                            node_type = "Server";
                         else if (rank < p.servers + p.clients)
                            node_type = "Client";
                         else
                            node_type = "REPL";

                         os << node_type << " Node " << rank << " log:\n";

                         run_cmd(os, cmd);
                     }
                 },
                 "Display the contents of the process's log file"
    );


    boost::asio::io_context ios;
    auto cli = cli::Cli(std::move(menu));
    cli.ExitAction([](auto& out) { out << "Goodbye.\n"; });
    cli::CliLocalTerminalSession localSession(cli, ios, std::cout, 200);
    localSession.ExitAction(
            [&ios, &app](auto& out) // session exit action
            {
                out << "Closing App...\n";
                ios.stop();
                if (app) {
                    app->rdbuf()->kill();
                    app.reset();
                }
            }
    );
    auto work = boost::asio::make_work_guard(ios);
    ios.run();
}

//
// Created by bparsy on 10/13/20.
//

#ifndef ALGOREP_PROCESSES_HH
#define ALGOREP_PROCESSES_HH

// REPL
#include "REPL.hh"

// Servers
#include "server/Server.hh"
#include "server/UIDProbe.hh"
#include "server/MultiProbe.hh"
#include "server/Paxos.hh"

// Clients
#include "client/Client.hh"
#include "client/UIDClient.hh"
#include "client/MultiClient.hh"

#endif //ALGOREP_PROCESSES_HH

//
// Created by bparsy on 10/14/20.
//

#ifndef ALGOREP_MESSAGE_HH
#define ALGOREP_MESSAGE_HH

enum message_tag {
    // REPL <-> Server
    MSG_START,
    MSG_CRASH,
    MSG_SPEED,
    MSG_RECOVER,

    // Client <-> Server
    MSG_DATA_PACKET,
    MSG_PENDING,
    MSG_RECEIVED,
    MSG_FINISHED,

    // Server <-> Server (PAXOS)
    MSG_PREPARE_PACKET,
    MSG_PROMISE_PACKET,
    MSG_ACCEPT_PACKET,
    MSG_ANSWER_PACKET,
    MSG_RECOVERY_PACKET,
};

#endif //ALGOREP_MESSAGE_HH

//
// Created by bparsy on 11/23/20.
//

#include "REPL.hh"

REPL::REPL(mpi::communicator local) : Process(std::move(local)) {}

void REPL::run() {
    std::string line;
    while (getline(std::cin, line)) {
        if (line == "start") {
            commit() << "Starting clients";
            for (auto i = servers_; i < servers_ + clients_; ++i)
                world_.isend(i, MSG_START);
        } else if (line == "crash") {
            int server;
            std::cin >> server;
            if (server < 0 || (long) server >= (long) servers_) {
                commit() << "Could no crash server " << server << ": does not exist";
                continue;
            }
            commit() << "Sending crash command to server " << server;
            world_.isend(server, MSG_CRASH);
        } else if (line == "speed") {
            int server;
            std::cin >> server;
            if (server < 0 || (long) server >= (long) servers_) {
                commit() << "Could no change speed of server " << server << ": does not exist";
                continue;
            }
            int speed_lvl;
            std::cin >> speed_lvl;
            commit() << "Sending speed command to server " << server;
            world_.isend(server, MSG_SPEED, speed_lvl);
        } else if (line == "recover") {
            int server;
            std::cin >> server;
            if (server < 0 || (long) server >= (long) servers_) {
                commit() << "Could no recover server " << server << ": does not exist";
                continue;
            }
            commit() << "Sending recover command to server " << server;
            world_.isend(server, MSG_RECOVER);
        } else if (line.empty()) {
            continue;
        } else {
            commit() << "Unrecognized REPL command: " << line;
        }
    }
    commit() << "finished";
}

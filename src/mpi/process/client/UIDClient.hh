//
// Created by bparsy on 10/14/20.
//

#ifndef ALGOREP_UIDCLIENT_HH
#define ALGOREP_UIDCLIENT_HH


#include "Client.hh"

class UIDClient : public Client {
public:
    explicit UIDClient(mpi::communicator local);
    ~UIDClient() override = default;
    void run() override;
};


#endif //ALGOREP_UIDCLIENT_HH

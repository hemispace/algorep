//
// Created by bparsy on 11/23/20.
//

#ifndef ALGOREP_MULTICLIENT_HH
#define ALGOREP_MULTICLIENT_HH


#include <random>
#include "Client.hh"

constexpr static int CLIENT_TIMEOUT_TIME = 3;
constexpr static int IGNORE_AFTER_TIMEOUT_TIME = 10;

class MultiClient : public Client {
public:
    explicit MultiClient(mpi::communicator local, size_t request_number);
    ~MultiClient() override = default;
    void run() override;
protected:
    size_t request_number_{10};
    mpi::timer timeout_timer_{};
    std::vector<std::optional<mpi::timer>> timeout_servers_;
};


#endif //ALGOREP_MULTICLIENT_HH

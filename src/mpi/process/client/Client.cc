//
// Created by bparsy on 10/13/20.
//

#include "Client.hh"

Client::Client(mpi::communicator local) : Process(std::move(local)) {}

void Client::run() {
    std::cout << "I am a client" << std::endl;
}

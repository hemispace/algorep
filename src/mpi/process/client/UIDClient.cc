//
// Created by bparsy on 10/14/20.
//

#include "UIDClient.hh"

UIDClient::UIDClient(mpi::communicator local) : Client(std::move(local)) {}

void UIDClient::run() {
    auto dest_server = world_.rank() % Process::servers_;
    world_.isend(dest_server, MSG_DATA_PACKET, world_.rank());
    info() << "client " << world_.rank() << ": sent '" << world_.rank() << "' to server " << dest_server;

    world_.recv(dest_server, MSG_RECEIVED);
    info() << "client " << world_.rank() << ": received accept from server " << dest_server;

    int answer;
    world_.recv(dest_server, MSG_FINISHED, answer);
    info() << "client " << world_.rank() << ": received answer '" << answer << "' from server " << dest_server;
}


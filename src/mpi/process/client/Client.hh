//
// Created by bparsy on 10/13/20.
//

#ifndef ALGOREP_CLIENT_HH
#define ALGOREP_CLIENT_HH


#include "../Process.hh"

class Client : public Process {
public:
    explicit Client(mpi::communicator local);
    ~Client() override = default;
    void run() override;
};


#endif //ALGOREP_CLIENT_HH

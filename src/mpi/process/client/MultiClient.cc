//
// Created by bparsy on 11/23/20.
//

#include "MultiClient.hh"

MultiClient::MultiClient(mpi::communicator local, size_t request_number)
        : Client(std::move(local)), request_number_(request_number), timeout_servers_(Process::servers_) {}

void MultiClient::run() {
    auto start_msg = world_.probe(mpi::any_source, MSG_START);
    world_.recv(start_msg.source(), start_msg.tag());


    std::random_device rd;
    std::mt19937 gen(rd());

    auto rand_srv = std::uniform_int_distribution<int>(0, Process::servers_ - 1);
    auto rand_msg = std::uniform_int_distribution<uint64_t>(0, std::numeric_limits<uint64_t>::max());

    auto sent_messages = 0u;
    bool answered = false;
    auto send_msg = rand_msg(gen);
    while (sent_messages < request_number_) {
        auto dst = rand_srv(gen);
        // Change server if they have timed out recently
        while (timeout_servers_[dst]) {
            if (timeout_servers_[dst]->elapsed() > IGNORE_AFTER_TIMEOUT_TIME) {
                timeout_servers_[dst] = std::nullopt;
                break;
            }
            dst = rand_srv(gen);
        }

        // If the message was sent on the previous iteration, change it
        if (answered)
            send_msg = rand_msg(gen);
        answered = false;

        // Send random request to random server
        world_.isend(dst, MSG_DATA_PACKET, send_msg);
        info() << "client " << world_.rank() << ": sent '" << send_msg << "' to server " << dst;

        uint64_t answer;
        timeout_timer_.restart();
        // The server must keep the client alive at least every CLIENT_TIMEOUT_TIME seconds
        // or answer it or else the client timeouts and considers
        while (timeout_timer_.elapsed() < CLIENT_TIMEOUT_TIME && !answered) {
            auto recv_msg = world_.iprobe(dst);
            if (recv_msg) {
                switch (recv_msg->tag()) {
                    case MSG_RECEIVED:
                    case MSG_PENDING:
                        world_.recv(recv_msg->source(), recv_msg->tag());
                        timeout_timer_.restart();
                        break;
                    case MSG_FINISHED:
                        world_.recv(recv_msg->source(), recv_msg->tag(), answer);
                        answered = answer == send_msg >> 4u;
                        break;
                    default:
                        throw std::invalid_argument("MultiClient: Invalid message tag");
                }
            }
        }

        if (answered) {
            info() << "client " << world_.rank() << ": received right answer(" << answer << ") from server " << dst;
            ++sent_messages;
        } else {
            info() << "client " << world_.rank() << ": TIMEOUT, no accept received from  " << dst;
            commit() << sent_messages << " TIMEOUT";
            timeout_servers_[dst] = mpi::timer{};
        }
    }
}

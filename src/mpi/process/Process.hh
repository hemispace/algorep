//
// Created by bparsy on 10/13/20.
//

#ifndef ALGOREP_PROCESS_HH
#define ALGOREP_PROCESS_HH

#include <boost/mpi.hpp>
#include <optional>
#include "message.hh"
#include "logger/loggers.hh"

namespace mpi = boost::mpi;

class Process {
public:
    using ptr = std::unique_ptr<Process>;
    explicit Process(mpi::communicator local);
    virtual ~Process() = default;

    template<typename Server, typename Client, typename REPL>
    static auto make_process(unsigned int servers, unsigned int clients, unsigned int requests) -> ptr;

    virtual void run() = 0;
    friend class Logger;
    inline static InfoLogger info{};
    inline static CommitLogger commit{};
protected:
    inline static unsigned servers_{}, clients_{};
    inline static int repl_{};
    inline static mpi::environment env_{};
    inline static mpi::communicator world_{};
    mpi::communicator local_{};
};

class MultiClient;

template<typename Server, typename Client, typename REPL>
auto Process::make_process(unsigned int servers, unsigned int clients, unsigned int requests) -> Process::ptr {
    servers_ = servers;
    clients_ = clients;
    repl_ = world_.size() - 1;
    bool is_server = (unsigned) world_.rank() < servers;
    mpi::communicator local = world_.split(is_server ? 0 : 1);
    if (world_.rank() == repl_)
        return std::make_unique<REPL>(local);
    else if (is_server)
        return std::make_unique<Server>(local);
    else {
        if constexpr (std::is_same_v<Client, MultiClient>)
            return std::make_unique<Client>(local, requests);
        else
            return std::make_unique<Client>(local);
    }
}

#endif //ALGOREP_PROCESS_HH

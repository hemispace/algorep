//
// Created by bparsy on 11/2/20.
//

#ifndef ALGOREP_COMMITLOGGER_HH
#define ALGOREP_COMMITLOGGER_HH

#include "Logger.hh"

class CommitLogger : public Logger {
public:
    plog::lvl level() override;
};


#endif //ALGOREP_COMMITLOGGER_HH

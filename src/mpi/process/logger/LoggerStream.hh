//
// Created by bparsy on 11/2/20.
//

#ifndef ALGOREP_LOGGERSTREAM_HH
#define ALGOREP_LOGGERSTREAM_HH

#include <sstream>
#include "Logger.hh"

class LoggerStream {
public:
    explicit LoggerStream(Logger& logger);
    ~LoggerStream();
    template<typename T>
    LoggerStream& operator<<(T const& msg);
private:
    Logger& logger_;
    std::ostringstream oss_;
};


template<typename T>
LoggerStream& LoggerStream::operator<<(T const& msg) {
    oss_ << msg;
    return *this;
}

#endif //ALGOREP_LOGGERSTREAM_HH

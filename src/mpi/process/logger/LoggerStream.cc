//
// Created by bparsy on 11/2/20.
//

#include "LoggerStream.hh"

LoggerStream::LoggerStream(Logger& logger)
        : logger_(logger), oss_() {}

LoggerStream::~LoggerStream() {
    logger_.log(oss_.str());
}

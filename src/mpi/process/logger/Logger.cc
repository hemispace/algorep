//
// Created by bparsy on 11/2/20.
//

#include <boost/filesystem.hpp>
#include <utility>
#include <process/Process.hh>

#include "Logger.hh"
#include "LoggerStream.hh"

Logger& Logger::log(std::string const& msg) {
    for (auto const& os : streams_) {
        *os << level() << msg << std::endl;
    }
    return *this;
}

Logger& Logger::add_stream(plog::stream stream) {
    switch (stream) {
        case plog::stream::STDOUT:
            streams_.push_back(std::shared_ptr<std::ostream>(&std::cout, [](auto*) {}));
            break;
        case plog::stream::PROC_MAIN_FILE:
            const std::string logs_dirname("logs");

            boost::filesystem::path logs_path(logs_dirname.c_str());
            boost::filesystem::create_directory(logs_path);

            auto filename = logs_dirname + "/" + std::to_string(Process::world_.rank()) + ".log";
            auto file = std::make_shared<std::ofstream>(filename.c_str());
            streams_.push_back(std::move(file));
            break;
    }
    return *this;
}

LoggerStream Logger::operator()() {
    return LoggerStream(*this);
}

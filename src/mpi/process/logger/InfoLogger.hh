//
// Created by bparsy on 11/2/20.
//

#ifndef ALGOREP_INFOLOGGER_HH
#define ALGOREP_INFOLOGGER_HH


#include "Logger.hh"

class InfoLogger : public Logger {
public:
    plog::lvl level() override;
};


#endif //ALGOREP_INFOLOGGER_HH

//
// Created by bparsy on 11/2/20.
//

#ifndef ALGOREP_LOGGER_HH
#define ALGOREP_LOGGER_HH


#include <memory>
#include <vector>
#include <iostream>
#include <fstream>
#include "types.hh"

using ostream_ptr = std::shared_ptr<std::ostream>;

class LoggerStream;

class Logger {
public:
    Logger() = default;
    Logger& log(std::string const& msg);
    Logger& add_stream(plog::stream stream);
    LoggerStream operator()();

    virtual plog::lvl level() = 0;
protected:
    std::vector<ostream_ptr> streams_{};
};



#endif //ALGOREP_LOGGER_HH

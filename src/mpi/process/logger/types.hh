//
// Created by bparsy on 11/2/20.
//

#ifndef ALGOREP_TYPES_HH
#define ALGOREP_TYPES_HH

namespace plog {

    enum class stream {
        // Terminal output
        STDOUT,

        // File with replica logs that we'll be looking to see if all went well
        PROC_MAIN_FILE
    };

    enum class lvl {
        // Important log to be committed (generally to a file)
        // Used to save replicated logs
        COMMIT,

        // Info log about the general execution of processes (generally to stdout)
        INFO,

        // When something may be going wrong
        WARN,

        // When something has definitely gone wrong
        ERROR
    };

    inline std::ostream& operator<<(std::ostream& os, lvl lvl) {
        switch (lvl) {
            case lvl::COMMIT:
                return os << "[ COMMIT ] ";
            case lvl::INFO:
                return os << "[  INFO  ] ";
            case lvl::WARN:
                return os << "[  WARN  ] ";
            case lvl::ERROR:
                return os << "[ ERROR  ] ";
        }
        return os;
    }
}


#endif //ALGOREP_TYPES_HH

//
// Created by bparsy on 11/23/20.
//

#ifndef ALGOREP_REPL_HH
#define ALGOREP_REPL_HH


#include "Process.hh"

class REPL : public Process {
public:
    explicit REPL(mpi::communicator local);
    ~REPL() override = default;
    void run() override;
};


#endif //ALGOREP_REPL_HH

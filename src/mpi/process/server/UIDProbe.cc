//
// Created by bparsy on 10/14/20.
//

#include "UIDProbe.hh"

UIDProbe::UIDProbe(mpi::communicator local) : Server(std::move(local)) {}

void UIDProbe::run() {
    auto timer = mpi::timer{};
    while (timer.elapsed() < 0.1) {
        auto msg = world_.iprobe(mpi::any_source, MSG_DATA_PACKET);
        if (msg) {
            int uid;
            world_.recv(msg->source(), msg->tag(), uid);
            uids_.push(uid);
            std::cout << "server " << local_.rank() << ": received '" << uid
                      << "' from client " << msg->source() << ", sending accept back..." << std::endl;
            world_.isend(msg->source(), MSG_RECEIVED);
        }
    }
}


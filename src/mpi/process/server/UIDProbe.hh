//
// Created by bparsy on 10/14/20.
//

#ifndef ALGOREP_UIDPROBE_HH
#define ALGOREP_UIDPROBE_HH


#include <queue>
#include "Server.hh"

class UIDProbe : public Server {
public:
    explicit UIDProbe(mpi::communicator local);
    ~UIDProbe() override = default;
    void run() override;
protected:
    std::queue<int> uids_{};
};


#endif //ALGOREP_UIDPROBE_HH

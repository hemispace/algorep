//
// Created by bparsy on 10/16/20.
//

#include "Paxos.hh"

Paxos::Paxos(mpi::communicator const& local) : MultiProbe(local) {}

void Paxos::run() {
    committed_data_.clear();
    committed_data_.push_back(0);
    mpi::timer pending_client_refresh_timer{};

    while (true) {
        // Refresh clients that the servers still knows about but hasn't answered yet every seconds so they don't timeout
        if (pending_client_refresh_timer.elapsed() > 1) {
            refresh_pending_clients();
            pending_client_refresh_timer.restart();
        }

        remove_invalid_rounds();

        // Check for client requests
        auto request = world_.iprobe(mpi::any_source, MSG_DATA_PACKET);
        if (request) {
            handle_request(request.get());
            usleep(delay_);
        }

        // REPL messages
        auto repl_request = world_.iprobe(repl_, mpi::any_tag);
        if (repl_request) {
            switch (repl_request->tag()) {
                case MSG_CRASH:
                    crash(repl_request.get());
                    break;
                case MSG_SPEED:
                    speed(repl_request.get());
                    break;
                case MSG_RECOVER:
                    recover(repl_request.get());
                    break;
                default:
                    throw std::invalid_argument("Paxos: Unrecognized tag from REPL");
            }
            usleep(delay_);
        }

        // Server messages (Paxos core)
        auto msg = local_.iprobe();
        if (msg) {
            switch (msg->tag()) {
                case MSG_PREPARE_PACKET:
                    prepare(msg.get());
                    break;
                case MSG_PROMISE_PACKET:
                    promise(msg.get());
                    break;
                case MSG_ACCEPT_PACKET:
                    accept(msg.get());
                    break;
                case MSG_ANSWER_PACKET:
                    answer(msg.get());
                    break;
                case MSG_RECOVERY_PACKET:
                    recovery_assist(msg.get());
                    break;
                default:
                    throw std::invalid_argument("Paxos: Invalid message tag");
            }
        }
        usleep(delay_);
    }
}

bool Paxos::majority(int votes) const {
    static const auto majority = local_.size() / 2 + 1;
    return votes >= majority;
}

uint64_t Paxos::proposal(uint64_t round, uint64_t rank) {
    // Concatenate round (first 32 bits) with rank (last 32 bits)
    return round << 32u | (rank & 0xFFFFFFFF);
}

round_state& Paxos::get_round_state(uint64_t proposal) {
    const uint64_t proposal_round = proposal >> 32u;
    if (proposal_round > round_) round_ = proposal_round;
    for (uint64_t round = max_committed_round_ + 1; round < proposal_round; ++round)
        round_states_[round];
    auto& state = round_states_[proposal_round];
    state.timer.restart();
    return state;
}

round_state* Paxos::get_recent_round_state(uint64_t proposal) {
    if (proposal >> 32u > max_committed_round_)
        return &get_round_state(proposal);
    // If we already committed a round smaller or equal than the current one, it is "too old"
    return nullptr;
}

void Paxos::handle_request(mpi::status const& msg) {
    uint64_t data;
    world_.recv(msg.source(), msg.tag(), data);

    // A dead server does not answer its clients
    if (crashed_) return;

    client_data_.push_back({msg.source(), data});
    info() << "server " << local_.rank() << ": received '" << data << "' from client " << msg.source()
           << ", sending accept back...";
    world_.isend(msg.source(), MSG_RECEIVED);

    // We received a new request, let's try to propose it
    attempt_new_round();
}

void Paxos::refresh_pending_clients() {
    // A dead server is a server that does not tell its clients that it's alive
    if (crashed_) return;

    for (auto const& data : client_data_)
        world_.isend(data.uid, MSG_PENDING);
}

void Paxos::attempt_commit() {
    for (auto round = round_states_.cbegin(); round != round_states_.cend();) {
        if (auto winner = round->second.learner.vote_result(local_.size() / 2); winner) {
            commit() << winner.value();
            committed_data_.push_back(winner.value());
            if (max_committed_round_ < round->second.min_proposal >> 32u)
                max_committed_round_ = round->second.min_proposal >> 32u;
            round = round_states_.erase(round);

            if (!client_data_.empty() && client_data_.front().msg == winner.value()) {
                world_.isend(client_data_.front().uid, MSG_FINISHED, winner.value() >> 4u);
                client_data_.pop_front();
            }
        } else if (round->second.crashed) {
            info() << 0;
            committed_data_.push_back(0);
            if (max_committed_round_ < round->second.min_proposal >> 32u)
                max_committed_round_ = round->second.min_proposal >> 32u;
            round = round_states_.erase(round);
        } else {
            auto[value, count] = round->second.learner.current_winner();
            info() << "server " << local_.rank() << ": waiting more answers for " << value << " (" << count <<
                   " votes), round=" << round->first << ", id=" << round->second.min_proposal;
            break;
        }
    }

    if (round_states_.empty()) {
        node_state_.state = node_state::WAITING;
        info() << "server " << local_.rank() << ": can initiate new paxos";
        attempt_new_round();
    }
}

bool Paxos::handle_vote(uint64_t proposal, uint64_t value, int rank) {
    auto count = get_round_state(proposal).learner.value_vote(value, rank);
    info() << "server " << local_.rank() << ": ack " << count << "th vote from " << rank << ", value=" << value
           << ", round=" << (proposal >> 32u);

    bool majority_obtained = majority(count);
    if (majority_obtained) {
        info() << "server " << local_.rank() << ": a majority has been obtained on " << value;
        attempt_commit();
    }

    return !majority_obtained;
}

// Phase 1: Step 1 & 2
void Paxos::attempt_new_round() {
    // To become a proposer, a server must have something to propose and no ongoing propositions
    if (client_data_.empty() || node_state_.state != node_state::WAITING) return;

    ++round_;
    // Chosing proposal number n (Step 1)
    auto proposal = Paxos::proposal(round_, local_.rank());
    round_states_[round_] = {proposal, {}};
    // Broadcast prepare(n) (Step 2)
    for (auto i = 0; i < local_.size(); ++i)
        local_.isend(i, MSG_PREPARE_PACKET, proposal);
    info() << "proposer " << local_.rank() << ": sent prepare(" << proposal << "), round=" << round_;
    node_state_ = {node_state::PROPOSING, proposal, 0, {0, 0}};
    // The proposer sent his prepare and now needs to receive a majority of votes to go on
}

// Acceptor response to prepare (Step 3)
void Paxos::prepare(mpi::status const& msg) {
    uint64_t n;
    local_.recv(msg.source(), msg.tag(), n);

    // A dead server does not send prepare message
    if (crashed_) return;

    info() << "acceptor " << local_.rank() << ": recv prepare(" << n << ") from proposer " << msg.source();

    // Checking if the round is not too old
    if (auto* state = get_recent_round_state(n); state != nullptr) {
        if (n > state->min_proposal) {
            state->min_proposal = n;
            node_state_.state = node_state::ACCEPTING;
            local_.isend(msg.source(), MSG_PROMISE_PACKET, *state);
            info() << "acceptor " << local_.rank() << ": sent promise(" << state->accepted << ") to proposer "
                   << msg.source();
            return;
        }
    }
    info() << "acceptor " << local_.rank() << ": ignored proposal from " << msg.source();
}

// Propose counts responses (votes) (Step 4)
void Paxos::promise(mpi::status const& msg) {
    round_state promise;
    local_.recv(msg.source(), msg.tag(), promise);

    if (crashed_) return;
    if (node_state_.state != node_state::PROPOSING) return;
    if (get_recent_round_state(promise.min_proposal) == nullptr) return;

    // The proposal number of the promise must be our current one
    if (promise.min_proposal != node_state_.proposal) {
        info() << "proposer " << local_.rank() << ": recv promise(" << promise.accepted << "), but it was too old ("
               << promise.min_proposal << "!=" << node_state_.proposal << ")";
        return;
    }

    info() << "proposer " << local_.rank() << ": recv promise(" << promise.accepted << ")";

    // Convert to someone else's proposal if it's bigger that yours
    if (promise.accepted.proposal > node_state_.accepted.proposal)
        node_state_.accepted = promise.accepted;

    node_state_.votes++;

    // We count ourselves in the vote (+1)
    if (majority(node_state_.votes + 1)) {
        auto pv = proposal_value{
                node_state_.proposal,
                node_state_.accepted.proposal > 0 ? node_state_.accepted.value : client_data_.front().msg
        };
        node_state_.state = node_state::ACCEPTING;
        handle_vote(promise.min_proposal, pv.value, local_.rank());

        // Broadcast accept(n , value) (Step 5)
        for (auto i = 0; i < local_.size(); ++i)
            local_.isend(i, MSG_ACCEPT_PACKET, pv);
        info() << "proposer " << local_.rank() << ": sent accept(" << pv << ")";
    }
}

void Paxos::accept(const mpi::status& msg) {
    proposal_value pv{};
    local_.recv(msg.source(), msg.tag(), pv);

    // A crashed server does not accept
    if (crashed_) return;

    info() << "acceptor " << local_.rank() << ": recv accept(" << pv << ") from proposer " << msg.source();

    if (auto* state = get_recent_round_state(pv.proposal); state != nullptr) {
        if (pv.proposal >= state->min_proposal) {
            state->min_proposal = pv.proposal;
            info() << "acceptor " << local_.rank() << ": sent answer(" << pv << ")";

            if (handle_vote(pv.proposal, pv.value, local_.rank()))
                handle_vote(pv.proposal, pv.value, msg.source());

            // Broadcast answer to accept (Step 6)
            for (auto i = 0; i < local_.size(); ++i)
                local_.isend(i, MSG_ANSWER_PACKET, pv);
        } else {
            info() << "acceptor " << local_.rank() << ": voted(" << pv << ") for " << msg.source();
            handle_vote(pv.proposal, pv.value, msg.source());
        }
    } else
        info() << "acceptor " << local_.rank() << ": ignored accept from " << msg.source()
               << " (round too low: " << (pv.proposal >> 32u) << "<=" << max_committed_round_ << ")";
}

// Proposer receives answers from accept (Step 7)
void Paxos::answer(const mpi::status& msg) {
    proposal_value pv;
    local_.recv(msg.source(), msg.tag(), pv);

    // A crashed server ignores accept answers
    if (crashed_) return;

    info() << "proposer " << local_.rank() << ": recv answer(" << pv << ") from acceptor " << msg.source();

    if (auto* state = get_recent_round_state(pv.proposal); state != nullptr) {
        state->min_proposal = std::max(state->min_proposal, pv.proposal);
        state->accepted = pv;
        // commits take place in handle_vote for both proposers and acceptors, as soon as there is a majority
        handle_vote(pv.proposal, pv.value, msg.source());
    } else
        info() << "server " << local_.rank() << ": refused answer(" << pv << ") fromm acceptor " << msg.source();
}

void Paxos::crash(const mpi::status& msg) {
    world_.recv(msg.source(), msg.tag());
    crashed_ = true;

    // Delete all data
    round_states_.clear();
    client_data_.clear();
    node_state_.state = node_state::WAITING;

    info() << "server " << local_.rank() << ": crashed (expectedly)";
}

void Paxos::speed(const mpi::status& msg) {
    int speed_lvl;
    world_.recv(msg.source(), msg.tag(), speed_lvl);

    // HIGH
    if (speed_lvl == 1) {
        delay_ = 1;
        info() << "server " << local_.rank() << ": set speed to HIGH";
    }
        // MEDIUM
    else if (speed_lvl == 2) {
        delay_ = 10;
        info() << "server " << local_.rank() << ": set speed to MEDIUM";
    }
        // LOW
    else if (speed_lvl == 3) {
        delay_ = 50;
        info() << "server " << local_.rank() << ": set speed to LOW";
    } else
        info() << "Unrecognized speed level: " << speed_lvl;
}

void Paxos::recover(const mpi::status& msg) {
    world_.recv(msg.source(), msg.tag());

    // A server needs to have crashed to start a recover
    if (!crashed_) return;

    crashed_ = false;

    for (auto i = 0; i < local_.size(); ++i)
        local_.isend(i, MSG_RECOVERY_PACKET, (uint64_t) 0);

    info() << "server " << local_.rank() << ": initiating recovery attempt";
}

void Paxos::recovery_assist(const mpi::status& msg) {
    uint64_t round;
    local_.recv(msg.source(), msg.tag(), round);

    // A dead server does not answer for an assistance to help a node recover
    if (crashed_) return;

    if (round == 0 && committed_data_.size() > 1) {
        local_.isend(msg.source(), MSG_ANSWER_PACKET, proposal_value{
                Paxos::proposal(committed_data_.size() - 1, servers_),
                committed_data_.back()
        });
    } else if (round < committed_data_.size()) {
        local_.isend(msg.source(), MSG_ANSWER_PACKET, proposal_value{
                Paxos::proposal(round, servers_),
                committed_data_[round]
        });
    }
}

void Paxos::remove_invalid_rounds() {
    // Again, that's a no if the server crashed
    if (crashed_) return;

    std::vector<uint64_t> recovering_queue;
    std::size_t total_recoveries{0};
    uint64_t highest_recovery = std::numeric_limits<uint64_t>::max();
    bool attempt_to_commit{false};

    for (auto round = round_states_.begin(); round != round_states_.end(); ++round) {
        auto const n = round->first;
        auto& state = round->second;

        if (state.timer.elapsed() > RECOVERY_IDLE_TIME && !state.in_recovery &&
            total_recoveries < MAX_RECOVER_PER_ROUND) {
            ++total_recoveries;
            state.in_recovery = true;
            state.timer.restart();
            recovering_queue.push_back(n);
        } else if (state.in_recovery)
            ++total_recoveries;

        if (state.in_recovery)
            highest_recovery = n;
    }

    for (auto round = round_states_.begin(); round != round_states_.end(); ++round) {
        if (round->second.timer.elapsed() > RECOVERY_GIVEUP_TIME && round->first <= highest_recovery) {
            round->second.crashed = true;
            attempt_to_commit = true;
        }
    }

    for (auto round : recovering_queue) {
        for (int i = 0; i < local_.size(); ++i)
            local_.isend(i, MSG_RECOVERY_PACKET, round);
    }

    if (attempt_to_commit)
        attempt_commit();
}
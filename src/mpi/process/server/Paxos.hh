//
// Created by bparsy on 10/16/20.
//

#ifndef ALGOREP_PAXOS_HH
#define ALGOREP_PAXOS_HH


#include <unordered_set>
#include <map>
#include "MultiProbe.hh"

static constexpr size_t RECOVERY_IDLE_TIME = 2;
static constexpr size_t RECOVERY_GIVEUP_TIME = 15;
static constexpr size_t MAX_RECOVER_PER_ROUND = 5;

struct learner_node {
    std::unordered_map<uint64_t, std::unordered_set<int>> votes{};

    int value_vote(uint64_t value, int voter) {
        votes[value].insert(voter);
        return votes[value].size();
    }

    std::optional<uint64_t> vote_result(unsigned majority) const {
        auto winner = std::find_if(
                votes.cbegin(), votes.cend(),
                [majority](auto voters) { return voters.second.size() > majority; }
        );

        if (winner == votes.cend()) return std::nullopt;
        return winner->first;
    }

    auto current_winner() const {
        using int2 = std::pair<uint64_t, std::size_t>;
        return std::accumulate(votes.cbegin(), votes.cend(), int2{0, 0}, [](auto acc, auto x) {
            if (x.second.size() > acc.second) return int2{x.first, x.second.size()};
            return acc;
        });
    }
};

struct proposal_value {
    uint64_t proposal{0};
    uint64_t value{0};

    template<class Archive>
    void serialize(Archive& ar, [[maybe_unused]]const unsigned int version) {
        ar & proposal;
        ar & value;
    }

    friend std::ostream& operator<<(std::ostream& os, proposal_value const& pv) {
        return os << pv.proposal << ", " << pv.value;
    }
};

struct round_state {
    uint64_t min_proposal{0};
    proposal_value accepted{};
    learner_node learner{};

    // REPL
    bool crashed{false};
    mpi::timer timer{};
    bool in_recovery{false};

    template<class Archive>
    void serialize(Archive& ar, [[maybe_unused]]const unsigned int version) {
        ar & min_proposal;
        ar & accepted;
    }

    using map = std::map<uint64_t, round_state>;
};

struct node_state {
    enum {
        WAITING,
        PROPOSING,
        ACCEPTING
    } state;

    uint64_t proposal{0};
    int votes{0};
    proposal_value accepted{0, 0};
};

// This Boost optimisation causes the program to segfault somehow
//BOOST_IS_MPI_DATATYPE(proposal_value)

class Paxos : public MultiProbe {
public:
    explicit Paxos(mpi::communicator const& local);
    ~Paxos() override = default;
    void run() override;

    [[nodiscard]] bool majority(int votes) const;
protected:
    [[nodiscard]] static uint64_t proposal(uint64_t round, uint64_t rank);

    round_state& get_round_state(uint64_t proposal);
    round_state* get_recent_round_state(uint64_t proposal);
    void handle_request(mpi::status const& msg);
    void refresh_pending_clients();
    void attempt_commit();
    bool handle_vote(uint64_t proposal, uint64_t value, int rank);
    void attempt_new_round();
    void prepare(mpi::status const& msg);
    void promise(mpi::status const& msg);
    void accept(mpi::status const& msg);
    void answer(mpi::status const& msg);
    void crash(mpi::status const& msg);
    void speed(mpi::status const& msg);
    void recover(mpi::status const& msg);
    void recovery_assist(mpi::status const& msg);
    void remove_invalid_rounds();
private:
    uint64_t round_{}, max_committed_round_{};
    round_state::map round_states_{};
    node_state node_state_{node_state::WAITING};
    bool crashed_{false};
    // Delay in milliseconds between requests
    int delay_{1};
    // Data that the process committed from client requests
    std::vector<uint64_t> committed_data_{};
};


#endif //ALGOREP_PAXOS_HH

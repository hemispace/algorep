//
// Created by bparsy on 10/13/20.
//

#ifndef ALGOREP_SERVER_HH
#define ALGOREP_SERVER_HH


#include "../Process.hh"

class Server : public Process {
public:
    explicit Server(mpi::communicator local);
    ~Server() override = default;
    void run() override;
};


#endif //ALGOREP_SERVER_HH

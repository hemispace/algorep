//
// Created by bparsy on 11/23/20.
//

#ifndef ALGOREP_MULTIPROBE_HH
#define ALGOREP_MULTIPROBE_HH


#include <list>
#include "Server.hh"

struct client_data {
    int uid;
    uint64_t msg;
};

class MultiProbe : public Server {
public:
    explicit MultiProbe(mpi::communicator local);
    ~MultiProbe() override = default;
protected:
    std::list<client_data> client_data_;
};


#endif //ALGOREP_MULTIPROBE_HH

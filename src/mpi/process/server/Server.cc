//
// Created by bparsy on 10/13/20.
//

#include "Server.hh"

Server::Server(mpi::communicator local) : Process(std::move(local)) {}

void Server::run() {
    std::cout << "I AM A SERVER" << std::endl;
}

#include <boost/program_options.hpp>
#include <iostream>
#include "process/processes.hh"

namespace po = boost::program_options;

auto main(int argc, char* argv[]) -> int {
    unsigned clients, servers, requests_per_client;

    po::options_description desc("Options: ");
    desc.add_options()
            ("help,h", "Display this information")
            ("clients,c", po::value(&clients)->default_value(2u), "Number of clients")
            ("servers,s", po::value(&servers)->default_value(2u), "Number of servers")
            ("requests,r", po::value(&requests_per_client)->default_value(100u), "Number of requests per client");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    } catch (po::error const& e) {
        std::cerr << "Option error: " << e.what() << std::endl;
        return 2;
    }

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }
    mpi::communicator world;
    auto p = Process::make_process<Paxos, MultiClient, REPL>(servers, clients, requests_per_client);
    p->info.add_stream(plog::stream::STDOUT);
    p->commit.add_stream(plog::stream::STDOUT);
    p->commit.add_stream(plog::stream::PROC_MAIN_FILE);
    p->run();

    return 0;
}

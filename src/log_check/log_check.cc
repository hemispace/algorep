#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <unordered_map>

using namespace boost::filesystem;
namespace po = boost::program_options;

int log_check(std::string const& logs_path, unsigned servers) {
    try {
        if (!exists(logs_path)) {
            std::cerr << "Path: " << logs_path << " does not exist. Please verify the path and current directory and try again." << std::endl;
            return 1;
        }

        if (!is_directory(logs_path)) {
            std::cerr << "Path: " << logs_path << " is not a directory." << std::endl;
            return 1;
        }

        std::vector<std::string> files;
        std::map<std::string, std::vector<unsigned>> file_map;
        for (auto i = 0u; i < servers; ++i) {
            std::string file_path = std::to_string(i) + ".log";
            std::ifstream is(logs_path + file_path);

            if (!is.is_open()) {
                std::cerr << file_path << " not found. Are you sure the consensus was run with "
                    << servers << " servers? Continuing..." << std::endl;
            } else {
                std::string file((std::istreambuf_iterator<char>(is)),
                        std::istreambuf_iterator<char>());
                file_map[file].push_back(i);
            }
        }

        std::vector<std::vector<unsigned>> sorted_files;
        unsigned most_occurent_files_idx = 0;
        unsigned max_files = 0;
        for (auto pair: file_map) {
            sorted_files.push_back(pair.second);
        }

        for (auto i = 0u; i < sorted_files.size(); ++i)
        {
            if (sorted_files[i].size() > max_files) {
                most_occurent_files_idx = i;
                max_files = sorted_files[i].size();
            }
        }

        for (auto i: sorted_files[most_occurent_files_idx]) {
            std::string file_path = std::to_string(i) + ".log";
            std::cout << file_path << " [OK]" << std::endl;
        }

        for (auto i = 0u; i < sorted_files.size(); ++i) {
            if (i == most_occurent_files_idx)
                continue;

            for (auto j : sorted_files[i]) {
                std::string file_path = std::to_string(j) + ".log";
                std::cout << file_path << " [KO]" << std::endl;
            }
        }

    } catch (const filesystem_error& e){
        std::cout << e.what() << '\n';
        return 2;
    }

    return 0;
}

int log_diff(std::string const& logs_path, unsigned servers) {
    try {
        if (!exists(logs_path)) {
            std::cerr << "Path: " << logs_path << " does not exist. Please verify the path and current directory and try again." << std::endl;
            return 1;
        }

        if (!is_directory(logs_path)) {
            std::cerr << "Path: " << logs_path << " is not a directory." << std::endl;
            return 1;
        }

        std::vector<std::ifstream*> log_streams;
        std::vector<std::string> log_paths;
        for (auto i = 0u; i < servers; ++i) {
            std::string file_path = std::to_string(i) + ".log";
            auto* is = new std::ifstream(logs_path + file_path);

            if (!is->is_open()) {
                std::cerr << file_path << " not found. Are you sure the consensus was run with "
                    << servers << " servers? Continuing..." << std::endl;
            } else {
                log_streams.push_back(is);
                log_paths.push_back(file_path);
            }

        }

        bool diffs_found = false;
        for (auto i = 0u; i < log_streams.size(); ++i) {
            std::ifstream* is1 = log_streams[i];

            for (auto j = i; j < log_streams.size(); ++j) {
                if (i == j)
                    continue;
                std::ifstream* is2 = log_streams[j];

                is1->clear();
                is2->clear();
                is1->seekg (0, is1->beg);
                is2->seekg (0, is2->beg);

                size_t line_count = 0;
                std::string line1, line2;
                while (!is1->eof() && !is2->eof()) {
                    std::getline(*is1, line1);
                    std::getline(*is2, line2);

                    if (line1 != line2) {
                        if (!diffs_found) {
                            std::cout << "Diffs found:" << std::endl;
                            std::cout << "------------" << std::endl;
                            diffs_found = true;
                        }

                        std::cout << "Files " << log_paths[i] << " - "
                            << log_paths[j] << ", l." << line_count << std::endl;
                        std::cout << log_paths[i] << std::endl;
                        std::cout << "< " << line1 << std::endl; 
                        std::cout << log_paths[j] << std::endl;
                        std::cout << "> " << line2 << std::endl; 
                        std::cout << std::endl;

                        break;
                    }

                    line_count++;
                }

                if (!(is1->eof() && is2->eof())) {
                    if (!diffs_found) {
                        std::cout << "Diffs found:" << std::endl;
                        std::cout << "------------" << std::endl;
                        diffs_found = true;
                    }
                    std::cout << "Files " << log_paths[i] << " - "
                        << log_paths[j] << ", Unexpected EOL at l."
                        << line_count << " of file ";
                    if (!(is1->eof())) {
                        std::cout << log_paths[j] << std::endl;
                        std::cout << log_paths[i] << std::endl;
                        std::cout << "< " << line1 << std::endl; 
                        std::cout << log_paths[j] << std::endl;
                        std::cout << "> " << std::endl; 
                        std::cout << std::endl;
                    }
                    else if (!(is2->eof())) {
                        std::cout << log_paths[i] << std::endl;
                        std::cout << log_paths[i] << std::endl;
                        std::cout << "< " << std::endl; 
                        std::cout << log_paths[j] << std::endl;
                        std::cout << "> " <<  line2 << std::endl; 
                        std::cout << std::endl;
                    }

                }
            }

        }
        for (auto* is : log_streams)
            delete is;

        if (!diffs_found)
            std::cout << "Congratulation ! No diffs were found." << std::endl;

    } catch (const filesystem_error& e){
        std::cout << e.what() << '\n';
        return 2;
    }


    return 0;

}

int log_progress(std::string const& logs_path, unsigned servers, unsigned clients, unsigned requests_per_client) {
    unsigned total_commits = 0;

    try {
        if (!exists(logs_path)) {
            std::cerr << "Path: " << logs_path << " does not exist. Please verify the path and current directory and try again." << std::endl;
            return 1;
        }

        if (!is_directory(logs_path)) {
            std::cerr << "Path: " << logs_path << " is not a directory." << std::endl;
            return 1;
        }

        std::cout << "Progress per server:" << std::endl;
        std::cout << "--------------------" << std::endl;
        for (auto i = 0u; i < servers; ++i) {
            std::string file_path = std::to_string(i) + ".log";
            std::ifstream is(logs_path + file_path);

            int number_of_lines = 0;
            std::string line;
            while (std::getline(is, line))
                ++number_of_lines;

            total_commits += number_of_lines;
            std::cout << "[" << i << "]: "
                      << 100 * float(number_of_lines) / float(clients * requests_per_client) 
                      << "%" << std::endl;
        }

        float current_progress = 100 * float(total_commits) / float(servers * clients * requests_per_client);
        std::cout << "Found " << total_commits << " commits in " << servers << " log files." << std::endl
                  << "Overall progress: " << current_progress << "%" << std::endl;

    } catch (const filesystem_error& e){
        std::cout << e.what() << '\n';
        return 2;
    }
    return 0;
}

int log_cat(std::string const& logs_path, unsigned node_rank) {
    try {
        if (!exists(logs_path)) {
            std::cerr << "Path: " << logs_path << " does not exist. Please verify the path and current directory and try again." << std::endl;
            return 1;
        }

        if (!is_directory(logs_path)) {
            std::cerr << "Path: " << logs_path << " is not a directory." << std::endl;
            return 1;
        }

        std::string file_path = std::to_string(node_rank) + ".log";
        std::ifstream is(logs_path + file_path);
        if (!is.is_open()) {
            std::cerr << file_path 
                      << " not found. Are you sure the consensus was run with "
                      << "the correct number of nodes?" << std::endl;
        }

        std::string line;
        while (std::getline(is, line))
            std::cout << line << std::endl;


    } catch (const filesystem_error& e){
        std::cout << e.what() << '\n';
        return 2;
    }
    return 0;
}

int main(int argc, char* argv[]) {
    unsigned servers, clients, requests_per_client, node_rank;
    std::string logs_path;

    po::options_description desc("Options: ");
    desc.add_options()
        ("help,h", "Display this information")
        ("check,ch", "Check which logs are valid")
        ("diff,d", "Check diffs in logs")
        ("cat", "Display logs for given node")
        ("progress,pr", "Show current run progression")
        ("logs_path,p", po::value(&logs_path)->default_value("logs"), "Path to log directory")
        ("servers,s", po::value(&servers)->default_value(2u), "Number of servers")
        ("clients,c", po::value(&clients)->default_value(12u), "Number of clients")
        ("requests,r", po::value(&requests_per_client)->default_value(100u), "Number of requests per client")
        ("node,n", po::value(&node_rank)->default_value(0u), "Node rank for cat method");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    } catch (po::error const& e) {
        std::cerr << "Option error: " << e.what() << std::endl;
        return 2;
    }

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }

    logs_path += "/";

    if (vm.count("check"))
        return log_check(logs_path, servers);
    if (vm.count("diff"))
        return log_diff(logs_path, servers);
    if (vm.count("progress"))
        return log_progress(logs_path, servers, clients, requests_per_client);
    if (vm.count("cat"))
        return log_cat(logs_path, node_rank);
}
